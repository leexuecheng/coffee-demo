﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x804 上介绍了“空白页”项模板

namespace demo
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            myframe.Navigate(typeof(donut));
        }

        private void Mynav_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            switch(args.InvokedItem)
            {
                case "开始点单":myframe.Navigate(typeof(donut));break;
                case "咖啡选择":myframe.Navigate(typeof(coffee));break;
                case "选择日期":myframe.Navigate(typeof(schedule));break;
                case "点单号码":myframe.Navigate(typeof(complete));break;
            }
        }
    }
}
