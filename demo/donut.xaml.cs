﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

namespace demo
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class donut : Page
    {
        public donut()
        {
            this.InitializeComponent();
        }

        private void Originslider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider originslider = sender as Slider;
            if(originslider!=null)//当slider滑动的结果不为0是执行如下操作
            {
                originresult.Text = "your origin count is:"+Convert.ToString(originslider.Value);//将slider.value强制转换成string.
            }
        }

        private void Speedwayslider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider speedslider = sender as Slider;
            if(speedslider!=null)
            {
                speedwayresult.Text = "your speedway count is:" + Convert.ToString(speedslider.Value);
            }
        }
    }
}
